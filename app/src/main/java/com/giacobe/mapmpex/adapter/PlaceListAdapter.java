package com.giacobe.mapmpex.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.giacobe.mapmpex.R;
import com.giacobe.mapmpex.model.Place;

import java.util.List;

/**
 * Created by ITV01 on 6/25/17.
 */

public class PlaceListAdapter extends BaseAdapter {
    private Context mContext;
    private List<Place> placeList;

    public PlaceListAdapter(Context mContext, List<Place> placeList) {
        this.mContext = mContext;
        this.placeList = placeList;
    }

    @Override
    public int getCount() {
        return placeList.size();
    }

    @Override
    public Object getItem(int position) {
        return placeList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_list_place, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.txtPlaceAddress = (TextView) convertView.findViewById(R.id.txt_place_address);
            viewHolder.txtPlaceName = (TextView) convertView.findViewById(R.id.txt_place_name);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Place place = placeList.get(position);
        viewHolder.txtPlaceName.setText(place.getPlaceName());
        viewHolder.txtPlaceAddress.setText(place.getAddressName());
        return convertView;
    }

    class ViewHolder {
        private TextView txtPlaceName, txtPlaceAddress;
    }


}
