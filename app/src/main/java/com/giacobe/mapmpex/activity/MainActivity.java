package com.giacobe.mapmpex.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Toast;

import com.giacobe.mapmpex.R;
import com.giacobe.mapmpex.databinding.ActivityMainBinding;
import com.giacobe.mapmpex.model.Place;
import com.giacobe.mapmpex.utils.BaseActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

public class MainActivity extends BaseActivity implements android.location.LocationListener, View.OnClickListener, GoogleMap.OnMarkerClickListener{

    private static final int REQUEST_ID_ACCESS_COURSE_FINE_LOCATION = 100;
    // Millisecond
    private final long MIN_TIME_BW_UPDATES = 1000;
    // Met
    private final float MIN_DISTANCE_CHANGE_FOR_UPDATES = 1;
    private GoogleMap mMap;
    private ProgressDialog mapProgressDialog;
    ActivityMainBinding mActivityMainBinding;
    private List<Place> placeList;
    private LatLng currentLatLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mActivityMainBinding.toolbarMap.imgAddLocation.setOnClickListener(this);
        mActivityMainBinding.toolbarMap.imgListLocation.setOnClickListener(this);
        getListPlace();
        initMap();
    }

    private void initMap() {
        mapProgressDialog = showLoading(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                onMyMapReady(googleMap);
            }
        });
    }

    private void getListPlace() {
        placeList = getPlace();
    }

    private void onMyMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                hideLoading(mapProgressDialog);
                askPermissionsAndShowMyLocation();

            }
        });
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setMyLocationEnabled(true);
        mMap.setOnMarkerClickListener(this);
        for (int i = 0; i < placeList.size(); i++) {
            Marker placeMarker = createMarker(placeList.get(0));
        }
    }

    private void askPermissionsAndShowMyLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int accessCoarsePermisstion = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
            int accessFinePermisstion = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
            if (accessCoarsePermisstion != PackageManager.PERMISSION_GRANTED
            || accessFinePermisstion != PackageManager.PERMISSION_GRANTED){
                String[] permisstions = new String[]{
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                };
                ActivityCompat.requestPermissions(this, permisstions, REQUEST_ID_ACCESS_COURSE_FINE_LOCATION);
                return;
            }
        }
        showMyLocation();
    }

    private void showMyLocation() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        String locationProvider = getEnabledLocationProvider();
        if (locationProvider == null) {
            return;
        }

        Location myLocation = null;
        try {
            locationManager.requestLocationUpdates(locationProvider, MIN_TIME_BW_UPDATES,
                    MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
            myLocation = locationManager.getLastKnownLocation(locationProvider);
        } catch (SecurityException ex) {
            //catch with API >=23
            Toast.makeText(this, "Show My Location Error: " + ex.getMessage(), Toast.LENGTH_LONG).show();
            ex.printStackTrace();
            return;
        }

        if (myLocation != null) {
            LatLng latLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
            currentLatLng = latLng;
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13));
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLng)
                    .target(latLng)             // Sets the center of the map to location user
                    .zoom(15)                   // Sets the zoom
                    .bearing(90)                // Sets the orientation of the camera to east
                    .tilt(40)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder

            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            // Add marker to map:
            MarkerOptions option = new MarkerOptions();
            option.title("My Location");
            option.position(latLng);
            Marker currentMarker = mMap.addMarker(option);
            currentMarker.showInfoWindow();

        } else {
            Toast.makeText(this, "Location not found!", Toast.LENGTH_LONG).show();
        }



    }

    private Marker createMarker(Place place) {
        return mMap.addMarker(new MarkerOptions()
                .position(new LatLng(place.getLat(), place.getLng()))
                .anchor(0.5f, 0.5f)
                .title(place.getPlaceName()));
    }

    private String getEnabledLocationProvider() {
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        String bestProvider = locationManager.getBestProvider(criteria, true);
        boolean enabled = locationManager.isProviderEnabled(bestProvider);
        if (!enabled) {
            Toast.makeText(this, "No location provider enabled!", Toast.LENGTH_LONG).show();
            return null;
        }
        return bestProvider;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_ID_ACCESS_COURSE_FINE_LOCATION:
                if (grantResults.length > 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    this.showMyLocation();
                } else {
                    Toast.makeText(this, "Permission denied!", Toast.LENGTH_SHORT).show();
                }
        }
    }


    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_add_location:
                Intent mIntent = new Intent(this, LocationActivity.class);
                mIntent.putExtra("id", "add_place");
                startActivity(mIntent);
                break;
            case R.id.img_list_location:
                Intent intent = new Intent(this, LocationActivity.class);
                intent.putExtra("id", "list_place");
                intent.putExtra("lat", currentLatLng.latitude);
                intent.putExtra("lng", currentLatLng.longitude);
                startActivity(intent);
                break;
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        marker.showInfoWindow();
        return false;
    }
}
