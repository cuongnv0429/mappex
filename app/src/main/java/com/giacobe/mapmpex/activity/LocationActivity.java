package com.giacobe.mapmpex.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.giacobe.mapmpex.R;
import com.giacobe.mapmpex.databinding.ActivityLocationBinding;
import com.giacobe.mapmpex.fragment.FragmentAddLocation;
import com.giacobe.mapmpex.fragment.FragmentListPlace;
import com.giacobe.mapmpex.utils.BaseActivity;
import com.giacobe.mapmpex.utils.BaseFragment;

public class LocationActivity extends BaseActivity {

    ActivityLocationBinding mActivityLocationBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityLocationBinding = DataBindingUtil.setContentView(this, R.layout.activity_location);
        if (getIntent() != null) {
            switch (getIntent().getStringExtra("id")) {
                case "list_place":
                    pushFragment(FragmentListPlace.newInstance(getIntent().getDoubleExtra("lat", 0.0), getIntent().getDoubleExtra("lng", 0.0)));
                    break;
                case "add_place":
                    pushFragment(new FragmentAddLocation());
                    break;
            }
        }

    }

    public void pushFragment(BaseFragment mFragment) {
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.frane_location, mFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onBackPressed() {
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 1) {
            super.onBackPressed();
        } else {
            finish();
        }
    }

}
