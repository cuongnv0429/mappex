package com.giacobe.mapmpex.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.giacobe.mapmpex.R;
import com.giacobe.mapmpex.model.Place;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

/**
 * Created by ITV01 on 6/24/17.
 */

public class BaseActivity extends AppCompatActivity{

    private Realm mRealm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get a Realm instance for this thread
        mRealm = Realm.getDefaultInstance();
    }

    public Realm getmRealm() {
        if (mRealm == null) {
            mRealm = Realm.getDefaultInstance();
        }
        return mRealm;
    }

    public List<Place> getPlace() {
        List<Place> placeList;
        if (mRealm == null) {
            mRealm = Realm.getDefaultInstance();
        }
        placeList = mRealm.where(Place.class).findAll();
        if (placeList == null) {
            return new ArrayList<>();
        }
        return placeList;
    }


    public void savePlace(BaseActivity activity, final Place mPlace) {
        if (mRealm == null) {
            mRealm = Realm.getInstance(activity);
        }
        try {
//            mRealm.beginTransaction();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    Place mPlaceRealm = realm.createObject(Place.class);
                    mPlaceRealm.setAddressName(mPlace.getAddressName());
                    mPlaceRealm.setPlaceName(mPlace.getPlaceName());
                    mPlaceRealm.setLat(mPlace.getLat());
                    mPlaceRealm.setLng(mPlace.getLng());
                }
            });
//            mRealm.commitTransaction();
        } catch (Exception ex) {
            ex.printStackTrace();
            mRealm.cancelTransaction();
        }

    }

    public ProgressDialog showLoading(Context context) {
        ProgressDialog progressDialog = ProgressDialog.show(context, null, null, true);
        progressDialog.setContentView(R.layout.progress_loading);
        if(progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }

        progressDialog.show();
        return progressDialog;
    }

    public  void hideLoading(ProgressDialog progressDialog) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}
