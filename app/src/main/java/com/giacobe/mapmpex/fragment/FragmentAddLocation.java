package com.giacobe.mapmpex.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.giacobe.mapmpex.R;
import com.giacobe.mapmpex.adapter.PlaceArrayAdapter;
import com.giacobe.mapmpex.databinding.FragmentAddLocationBinding;
import com.giacobe.mapmpex.model.Place;
import com.giacobe.mapmpex.utils.BaseActivity;
import com.giacobe.mapmpex.utils.BaseFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

/**
 * Created by ITV01 on 6/25/17.
 */

public class FragmentAddLocation extends BaseFragment implements GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, View.OnClickListener{

    private BaseActivity mActivity;
    private FragmentAddLocationBinding mAddLocationBinding;
    private static final int GOOGLE_API_CLIENT_ID = 0;
    private GoogleApiClient mGoogleApiClient;
    private PlaceArrayAdapter mPlaceAdapter;
    //at location vietnam
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(14.058324, 108.277199), new LatLng(14.058324, 108.277199));
    private Place mPlace;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mAddLocationBinding == null) {
            mAddLocationBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_location, container, false);
            initGoogleApiClient();
            initView();
        }

        return mAddLocationBinding.getRoot();
    }

    private void initView() {
        mPlace = new Place();
        mAddLocationBinding.autoCompleteLocation.setThreshold(3);
        mPlaceAdapter = new PlaceArrayAdapter(mActivity, android.R.layout.simple_list_item_1, BOUNDS_MOUNTAIN_VIEW, null);
        mAddLocationBinding.autoCompleteLocation.setAdapter(mPlaceAdapter);
        mAddLocationBinding.autoCompleteLocation.setOnItemClickListener(mAutocompleteClickListener);
        mAddLocationBinding.btnOk.setOnClickListener(this);
        mAddLocationBinding.btnCancle.setOnClickListener(this);

    }


    private void initGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mActivity)
                .addApi(Places.GEO_DATA_API)
                .enableAutoManage(mActivity, GOOGLE_API_CLIENT_ID, this)
                .addConnectionCallbacks(this)
                .build();
    }

    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            PlaceArrayAdapter.PlaceAutocomplete item = mPlaceAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
        }
    };

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                return;
            }
            mPlace.setLng(places.get(0).getLatLng().longitude);
            mPlace.setLat(places.get(0).getLatLng().latitude);
            mPlace.setAddressName(places.get(0).getAddress().toString());
        }
    };


    @Override
    public void onConnected(Bundle bundle) {
        mPlaceAdapter.setGoogleApiClient(mGoogleApiClient);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        mPlaceAdapter.setGoogleApiClient(null);
    }

    private boolean validateData() {
        if (mAddLocationBinding.edtPlaceName.getText().toString().equalsIgnoreCase("") ||
                mAddLocationBinding.autoCompleteLocation.getText().toString().equalsIgnoreCase("")) {
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancle:
                mActivity.onBackPressed();
                break;
            case R.id.btn_ok:
                if (validateData()) {
                    Toast.makeText(mActivity, getResources().getString(R.string.validate_empty_infor), Toast.LENGTH_SHORT).show();
                } else {
                    mPlace.setPlaceName(mAddLocationBinding.edtPlaceName.getText().toString().trim());
                    mActivity.savePlace(mActivity, mPlace);
                    mActivity.onBackPressed();
                }
                break;
        }
    }
}
