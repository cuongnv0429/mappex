package com.giacobe.mapmpex.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.giacobe.mapmpex.R;
import com.giacobe.mapmpex.adapter.PlaceListAdapter;
import com.giacobe.mapmpex.databinding.FragmentListPlaceBinding;
import com.giacobe.mapmpex.model.Place;
import com.giacobe.mapmpex.utils.BaseActivity;
import com.giacobe.mapmpex.utils.BaseFragment;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by ITV01 on 6/25/17.
 */

public class FragmentListPlace extends BaseFragment implements View.OnClickListener{
    BaseActivity mActivity;
    FragmentListPlaceBinding mListPlaceBinding;

    private LatLng currentLatLng;
    private List<Place> placeList;
    private PlaceListAdapter mAdapter;

    public static FragmentListPlace newInstance(double lat, double lng) {
        Bundle args = new Bundle();
        args.putDouble("lat", lat);
        args.putDouble("lng", lng);
        FragmentListPlace fragment = new FragmentListPlace();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            currentLatLng = new LatLng(getArguments().getDouble("lat"), getArguments().getDouble("lng"));
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mListPlaceBinding == null) {
            mListPlaceBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_list_place, container, false);
            initView();
        }
        return mListPlaceBinding.getRoot();
    }

    private void initView() {
        mListPlaceBinding.imgBackListPlace.setOnClickListener(this);

        placeList = mActivity.getPlace();
        mAdapter = new PlaceListAdapter(mActivity, placeList);
        mListPlaceBinding.listviewPlace.setAdapter(mAdapter);
    }

    private float distanceCompareCurrentPlace(Place place1) {
        float[] results = new float[1];
        Location.distanceBetween(place1.getLat(), place1.getLng(),
                currentLatLng.latitude, currentLatLng.longitude,
                results);
        return results[0];

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back_list_place:
                mActivity.onBackPressed();
                break;
        }
    }
}
